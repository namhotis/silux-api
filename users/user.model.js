const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    username: { type: String, unique: true, required: true },
    hash: { type: String, required: true },
    createdDate: { type: Date, default: Date.now },
    totalDepense: { type: Number, default: 0 },
    GreenPoints: { type: Number, default: 0 },
    isConnected: {type: Boolean, default: false},
    isAdmin: {type: Boolean, default: true},
});

userSchema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('User', userSchema);