'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var ProductSchema = new Schema({
  name: {
    type: String,
    required: 'Veuillez entrer le nom du produit'
  },
  Created_date: {
    type: Date,
    default: Date.now
  },
  priceGramme: {
    type: Number,
    required: 'Veuillez entrer le prix d\'un gramme du produit'
  }
});

module.exports = mongoose.model('Product', ProductSchema);