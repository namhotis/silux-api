'use strict';

const express = require('express');
const router = express.Router();
const productService = require('./product.service');

router.get('/', list_all_products);
router.get('/:id', list_one_product);
router.post('/', register_one_product);
router.put('/:id', update_product);
router.delete('/:id', _delete_product);
router.get('/name/:name', list_one_product_by_name);

module.exports = router;

function list_all_products(req, res, next) {
  productService.getAll()
    .then(products => {
      if(products) {
        res.json(products)
      } else {
        res.status(400).json({ message: 'Bad request'})
      }
    })
    .catch(err => next(err));
};

function list_one_product(req, res, next) {
  productService.getById(req.params.id)
    .then(products => {
      if(products) {
        res.json(products)
      } else {
        res.status(400).json({ message: 'Ce product n\'a pas été trouvé' })
      }
    })
    .catch(err => next(err));
};

function register_one_product(req, res, next) {
  productService.create(req.body)
  .then(() => {
    console.log(req.body);
    productService.getByName(req.body.name)
    .then(products => {
      if(products) {
        res.json(products)
      } else {
        res.status(400).json({ message: 'Cet produit n\'a pas été trouvé' })
      }
    })
    .catch(err => next(err));
})
.catch(err => next(err));
}


function list_one_product_by_name(req, res, next) {
  productService.getByName(req.params.name)
    .then(products => {
      if(products) {
        res.json(products)
      } else {
        res.status(400).json({ message: 'Cet produit n\'a pas été trouvé' })
      }
    })
    .catch(err => next(err));
};

function update_product(req, res, next) {
  productService.update_product(req.params.id, req.body)
  .then(() => {
    productService.getById(req.params.id)
    .then((product) => {
        product ? res.json(product) : res.sendStatus(404)
    })
    .catch(err => next(err));
  })
  .catch(err => next(err));
}

function _delete_product(req, res, next) {
  productService.delete(req.params.id)
      .then(() => res.json({}))
      .catch(err => next(err));
}
