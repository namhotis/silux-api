'use strict';
const db = require('../../_helpers/db');
const Product = db.Product;

module.exports = {
    getAll,
    getById,
    create,
    update_product,
    delete: _delete_product,
    getByName
};

async function getAll() {
    return await Product.find();
}

async function getById(id) {
    return await Product.findById(id);
}

async function create(productParam) {
    // validate
    if (await Product.findOne({ name: productParam.name })) {
        throw 'Product "' + productParam.name + '" is already taken';
    }

    const product = new Product(productParam);

    // save user
    await product.save();
}


async function update_product(id, productParam) {
    const product = await Product.findById(id);

    // validate
    if (!product) throw 'Product not found';
    if (product.name !== productParam.name && await Product.findOne({ name: productParam.name })) {
        throw 'Product name "' + productParam.name + '" is already taken';
    }

    // copy userParam properties to user
    Object.assign(product, productParam);

    await product.save();
}

async function _delete_product(id) {
    await Product.findByIdAndRemove(id);
}


async function getByName(name) {
    return await Product.findOne({name: name});
}