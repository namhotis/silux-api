
'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var SiloSchema = new Schema({
  name: {
    type: Number,
    required: 'Veuillez entrer le numéro du silo'
  },
  isOpen: {
    type: Boolean,
    default: false
  },
  idProductInside: {
    type: String,
    required: 'Veuillez entrer l\id du produit contenu'
  },
  poidsPrecedent: {
    type: Number,
    default: '2430'
  },
  poidsActuel: {
    type: Number,
    default: null
  },
  QRCode: {
    type: String
  },
  poidsInitial: {
    type: Number
  }
});


module.exports = mongoose.model('Silo', SiloSchema);