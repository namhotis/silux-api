'use strict';
const db = require('../../_helpers/db');
const Silo = db.Silo;

module.exports = {
    getAll,
    getById,
    create,
    update_silo,
    delete: _delete_silo,
    getByQR,
};

async function getAll() {
    return await Silo.find();
}

async function getById(id) {
    return await Silo.findById(id);
}

async function create(siloParam) {
    // validate
    if (await Silo.findOne({ name: siloParam.name })) {
        throw 'Silo "' + siloParam.name + '" is already taken';
    }

    const silo = new Silo(siloParam);

    // save user
    await silo.save();
}


async function update_silo(id, siloParam) {
    const silo = await Silo.findById(id);

    // validate
    if (!silo) throw 'Silo not found';
    if (silo.name !== siloParam.name && await Silo.findOne({ name: siloParam.name })) {
        throw 'Silo name "' + siloParam.name + '" is already taken';
    }

    // copy userParam properties to user
    Object.assign(silo, siloParam);

    await silo.save();
}

async function _delete_silo(id) {
    await Silo.findByIdAndRemove(id);
}

async function getById(id) {
    return await Silo.findById(id);
}

async function getByQR(qr) {
    return await Silo.findOne({QRCode: qr});
}