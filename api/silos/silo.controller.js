'use strict';

const express = require('express');
const router = express.Router();
const siloService = require('./silo.service');


router.get('/', list_all_silos);
router.get('/:id', list_one_silo);
router.post('/', register_one_silo);
router.put('/:id', update_silo);
router.delete('/:id', _delete_silo);
router.get('/qr/:QRCode', list_one_silo_by_qr);

module.exports = router;

function list_all_silos(req, res, next) {
  siloService.getAll()
    .then(silos => {
      if(silos) {
        res.json(silos)
      } else {
        res.status(400).json({ message: 'Bad request'})
      }
    })
    .catch(err => next(err));
};

function list_one_silo(req, res, next) {
  siloService.getById(req.params.id)
    .then(silos => {
      if(silos) {
        res.json(silos)
      } else {
        res.status(400).json({ message: 'Ce silo n\'a pas été trouvé' })
      }
    })
    .catch(err => next(err));
};

function register_one_silo(req, res, next) {
  siloService.create(req.body)
  .then(() => {
    siloService.getById(req.params._id)
    .then((silo) => {
        silo ? res.json(silo) : res.sendStatus(404)
    })
    .catch(err => next(err));
})
.catch(err => next(err));
}

function update_silo(req, res, next) {
  siloService.update_silo(req.params.id, req.body)
  .then(() => {
    siloService.getById(req.params.id)   
    .then((silo) => {
        silo ? res.json(silo) : res.sendStatus(404)
    })
    .catch(err => next(err));
})
.catch(err => next(err));
}

function _delete_silo(req, res, next) {
  siloService.delete(req.params.id)
      .then(() => res.json({}))
      .catch(err => next(err));
}

function list_one_silo_by_qr(req, res, next) {
  siloService.getByQR(req.params.QRCode)
    .then(silos => {
      if(silos) {
        res.json(silos)
      } else {
        res.status(400).json({ message: 'Ce silo n\'a pas été trouvé' })
      }
    })
    .catch(err => next(err));
};