const express = require('express'),
  app = express(),
  port = process.env.PORT || 3000,
  mongoose = require('mongoose'),
  bodyParser = require('body-parser'),
  jwt = require('./_helpers/jwt'),
  cors = require('cors'),
  errorHandler = require('./_helpers/error-handler');
  
// mongoose instance connection url connection
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://heroku_gwc9t8rm:rk1nvjptcds856chi1h0n874ok@ds231537.mlab.com:31537/heroku_gwc9t8rm'); 


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

// app.use(jwt());

app.use('/users', require('./users/users.controller'));
app.use('/products', require('./api/products/product.controller'));
app.use('/silos', require('./api/silos/silo.controller'));

// global error handler
app.use(errorHandler);

app.listen(port);


console.log('Silux RESTful API server started on: ' + port);
